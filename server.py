#se importan y ejecutan todos los componentes de nustra aplicacion
from servicios import app
from servicios import db
from entidades import *
from verbos import *

#Si las entidades no existen y la base de datos esta vacia,se crena todas las tablas
#Ademas se inicia la aplicacion en localhost con el puerto 3000 (127.0.0.1:3000)
if __name__ == "__main__":
    db.create_all()
    app.run(port=3000, host="0.0.0.0")
