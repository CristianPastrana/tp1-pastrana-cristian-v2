import unittest
import requests


class ApiTest(unittest.TestCase):
    RENT_URL = "http://127.0.0.1:3000/rent/"

    def test_1_get_all_rents(self):
        r = requests.get(ApiTest.RENT_URL)
        self.assertEqual(r.status_code, 200)
        print("GET a todas las rentas, status code esperado: 200")
    
    def test_2_add_new_rent(self):
        xml= "<rent><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></rent>"
        r = requests.put(ApiTest.RENT_URL,xml)
        self.assertEqual(r.status_code, 201)
        print("PUT a una renta, status code esperado: 201")

    def test_3_update_new_rent(self):
        xml= "<rent><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>5d65ac9e-1111-2222-3333-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></rent>"
        r = requests.put(ApiTest.RENT_URL,xml)
        self.assertEqual(r.status_code, 204)
        print("PUT a una renta existente(update), status code esperado: 204")

    def test_4_get_new_rent(self):
        url = "http://127.0.0.1:3000/rent/b172a2ab-5900-4532-bd68-68a041752017"
        r = requests.get(url)
        self.assertEqual(r.status_code, 200)
        print("GET a una renta por object_id, status code esperado: 200")

    def test_5_get_empty_rent(self):
        url = "http://127.0.0.1:3000/rent/b172a2ab-bbbb-cccc-dddd-68a041752017"
        r = requests.get(url)
        self.assertEqual(r.status_code, 404)
        print("GET a una renta inexistente por object_id, status code esperado: 404")

    def test_6_add_new_rent_invalid_uuid (self):
        xml= "<rent><object_id>aaaaabbbbbccccccddddd</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></rent>"
        r = requests.put(ApiTest.RENT_URL,xml)
        self.assertEqual(r.status_code, 400)
        print("PUT a una renta con uuid invalido, status code esperado: 400")

    def test_7_add_new_rent_invalid_status (self):
        xml= "<rent><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENTED</status><until>2020/10/01</until></details></rent>"
        r = requests.put(ApiTest.RENT_URL,xml)
        self.assertEqual(r.status_code, 400)
        print("PUT a una renta con status invalido, status code esperado: 400")

    def test_8_add_new_rent_invalid_until (self):
        xml= "<rent><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENT</status><until>01/01/2001</until></details></rent>"
        r = requests.put(ApiTest.RENT_URL,xml)
        self.assertEqual(r.status_code, 400)
        print("PUT a una renta con until invalido, status code esperado: 400")

    def test_9_delete_rent(self):
        url = "http://127.0.0.1:3000/rent/b172a2ab-5900-4532-bd68-68a041752017"
        r = requests.delete(url)
        self.assertEqual(r.status_code, 204)
        print("DELETE a una renta , status code esperado: 204")
    
    def test_10_delete_empty_rent(self):
        url = "http://127.0.0.1:3000/rent/b172a2ab-5900-4532-bd68-68a041752017"
        r = requests.delete(url)
        self.assertEqual(r.status_code, 404)
        print("DELETE a una renta inexistente , status code esperado: 204")




if __name__ == "__main__":
    unittest.main()