from flask import jsonify, Response, request
from servicios import app, db
from entidades import *
import xml.etree.ElementTree as ET
import xmltodict
import uuid
from xml.parsers.expat import ExpatError



@app.route("/")
def server_info():
    rent= ET.Element("rent")
    server= ET.SubElement(rent,"server")
    server.text= "Trabajo Practico Inicial: API REST"

    xml= ET.tostring(rent)

    return Response(xml, mimetype='text/xml')
 


@app.route("/rent/<id>", methods=["GET"])
def list_renta(id):
    rent = Rent.query.get(id)
    if rent == None:
        return "no existe la renta", 404
    else:
        renta = ET.Element("renta")
        ET.SubElement(renta, "object_id").text = str(rent.object_id)
        ET.SubElement(renta, "client_id").text = str(rent.client_id)
        details = ET.SubElement(renta, "details")
        ET.SubElement(details, "status").text = str(rent.status)
        ET.SubElement(details, "until").text = str(rent.until)
        xml = ET.tostring(renta)

        return Response(xml, mimetype='text/xml'), 200

@app.route("/rent/", methods=["GET"])
def list_rentas():
    rents = Rent.query.order_by(Rent.object_id).all()
    rentas = ET.Element("rentas")

    for x in rents:
        renta = ET.SubElement(rentas, "renta")
        ET.SubElement(renta, "object_id").text = str(x.object_id)
        ET.SubElement(renta, "client_id").text = str(x.client_id)
        details = ET.SubElement(renta, "details")
        ET.SubElement(details, "status").text = str(x.status)
        ET.SubElement(details, "until").text = str(x.until)

    xml = ET.tostring(rentas)

    return Response(xml, mimetype='text/xml'), 200



def is_valid_uuid(val):
    try:
        uuid.UUID(str(val))
        return True
    except ValueError:
        return False

def is_valid_status(status):
    return status == "RENT" or status == "DELIVERY_TO_RENT" or status == "RETURN " or status == "DELIVERY_TO_RETURN "
        
from datetime import datetime

def is_valid_until(until):
    try:
        datetime.strptime(until, '%Y/%m/%d')
        return True
    except ValueError:
        return False

@app.route("/rent/", methods=["PUT"])
def new_rent():
    try:
        content_dict = xmltodict.parse(request.data)
    except ExpatError:
        return "XML vacio o formato invalido", 400
        
    object_id = content_dict["rent"]["object_id"]
    client_id = content_dict["rent"]["client_id"]
    status = content_dict["rent"]["details"]["status"]
    until = content_dict["rent"]["details"]["until"]


    update_renta = Rent.query.get(object_id)
    if update_renta == None:
        nueva_renta = Rent()
        if is_valid_uuid(object_id) and is_valid_uuid(client_id):
            nueva_renta.object_id = object_id
            nueva_renta.client_id = client_id
        else:
            return "uiid invalido", 400
        
        if is_valid_status(status):
            nueva_renta.status = status
        else:
            return "status invalido", 400
        if is_valid_until(until):
            nueva_renta.until = until
        else:
            return "Fecha invalida", 400
        
        db.session.add(nueva_renta)
        db.session.commit()
        xml= request.data
        return Response(xml, mimetype='text/xml'), 201
    else:
        if is_valid_uuid(object_id) and is_valid_uuid(client_id):
            update_renta.object_id = object_id
            update_renta.client_id = client_id
        else:
            return "uiid invalido", 400
        
        if is_valid_status(status):
            update_renta.status = status
        else:
            return "status invalido", 400
        if is_valid_until(until):
            update_renta.until = until
        else:
            return "Fecha invalida",400
        
        db.session.add(update_renta)
        db.session.commit()
        xml= request.data
        return Response(xml, mimetype='text/xml'), 204
        



@app.route("/rent/<id>", methods=["DELETE"])
def delete_renta(id):   
    renta = Rent.query.get(id)
    if renta == None:
        return  "no existe la renta que se quiere eliminar", 404
    else:
        db.session.delete(renta)
        db.session.commit()
        return id, 204


